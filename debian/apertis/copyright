Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: no-info-found
License: BSD-2-clause or GPL

Files: libfdt/*
Copyright:
 2006-2012 David Gibson, IBM Corporation.
 2012 Kim Phillips, Freescale Semiconductor.
 2014 David Gibson <david@gibson.dropbear.id.au>
 2016 Free Electrons
 2016 NextThing Co.
 2018 embedded brains GmbH
License: GPL-2+ or BSD-2-clause

Files: BSD-2-Clause
Copyright: no-info-found
License: BSD-2-clause

Files: GPL
Copyright: 1989, 1991, Free Software Foundation, Inc.
License: GPL-2

Files: Makefile
 Makefile.convert-dtsv0
 Makefile.dtc
 Makefile.utils
 dtdiff
 fdtdump.c
 meson.build
Copyright: no-info-found
License: GPL-2

Files: checks.c
Copyright: David Gibson <dwg@au1.ibm.com>, IBM Corporation. 2007.
License: GPL-2

Files: convert-dtsv0-lexer.l
Copyright: David Gibson <dwg@au1.ibm.com>, IBM Corporation. 2005, 2008.
License: GPL-2

Files: data.c
 dtc-lexer.l
 dtc-parser.y
 dtc.c
 dtc.h
 flattree.c
 fstree.c
 livetree.c
 treesource.c
Copyright: David Gibson <dwg@au1.ibm.com>, IBM Corporation. 2005.
License: GPL-2

Files: debian/*
Copyright: 2007, Aurélien GÉRÔME <ag@roxor.cx>
License: GPL-2+

Files: fdtget.c
 fdtput.c
Copyright: 2011, The Chromium OS Authors.
License: GPL-2

Files: fdtoverlay.c
Copyright: 2017, Konsulko Group Inc.
License: GPL-2

Files: scripts/*
Copyright: no-info-found
License: GPL-2

Files: setup.py
Copyright: 2017, Google, Inc.
License: GPL-2+

Files: srcpos.c
 srcpos.h
Copyright: 2007, Jon Loeliger, Freescale Semiconductor, Inc.
License: GPL-2

Files: tests/add_subnode_with_nops.c
 tests/appendprop1.c
 tests/appendprop2.c
 tests/asm_tree_dump.c
 tests/boot-cpuid.c
 tests/check_full.c
 tests/del_node.c
 tests/del_property.c
 tests/dtb_reverse.c
 tests/dtbs_equal_ordered.c
 tests/dtbs_equal_unordered.c
 tests/extra-terminating-null.c
 tests/find_property.c
 tests/get_alias.c
 tests/get_mem_rsv.c
 tests/get_name.c
 tests/get_path.c
 tests/get_phandle.c
 tests/get_prop_offset.c
 tests/getprop.c
 tests/incbin.c
 tests/integer-expressions.c
 tests/mangle-layout.c
 tests/move_and_save.c
 tests/node_check_compatible.c
 tests/node_offset_by_compatible.c
 tests/node_offset_by_phandle.c
 tests/node_offset_by_prop_value.c
 tests/nop_node.c
 tests/nop_property.c
 tests/nopulate.c
 tests/notfound.c
 tests/open_pack.c
 tests/parent_offset.c
 tests/path-references.c
 tests/path_offset.c
 tests/phandle_format.c
 tests/references.c
 tests/root_node.c
 tests/rw_oom.c
 tests/rw_tree1.c
 tests/set_name.c
 tests/setprop.c
 tests/setprop_inplace.c
 tests/string_escapes.c
 tests/subnode_offset.c
 tests/supernode_atdepth_offset.c
 tests/sw_tree1.c
 tests/tests.h
 tests/testutils.c
 tests/truncated_memrsv.c
 tests/truncated_property.c
 tests/truncated_string.c
 tests/value-labels.c
Copyright: 2006-2010, 2018, David Gibson, IBM Corporation.
License: LGPL-2.1

Files: tests/addr_size_cells.c
 tests/addr_size_cells2.c
Copyright: 2014, David Gibson, <david@gibson.dropbear.id.au>
License: LGPL-2.1

Files: tests/appendprop_addrrange.c
Copyright: 2018, AKASHI Takahiro, Linaro Limited
License: LGPL-2.1

Files: tests/char_literal.c
 tests/sized_cells.c
Copyright: 2011, The Chromium Authors.
 2006, David Gibson, IBM Corporation.
License: LGPL-2.1

Files: tests/check_header.c
Copyright: 2018, David Gibson <david@gibson.dropbear.id.au>
License: LGPL-2.1

Files: tests/check_path.c
Copyright: 2016, Konsulko Inc.
License: LGPL-2.1

Files: tests/dumptrees.c
Copyright: David Gibson <dwg@au1.ibm.com>, IBM Corporation. 2006.
License: GPL-2

Files: tests/fs_tree1.c
 tests/sw_states.c
Copyright: 2018, David Gibson, Red Hat Inc.
License: LGPL-2.1

Files: tests/get_next_tag_invalid_prop_len.c
Copyright: no-info-found
License: LGPL-2.1

Files: tests/overlay.c
 tests/overlay_bad_fixup.c
Copyright: 2016, NextThing Co.
 2016, Free Electrons
License: LGPL-2.1

Files: tests/overlay_bad_fixup_bad_index.dts
 tests/overlay_bad_fixup_base.dtsi
 tests/overlay_bad_fixup_empty.dts
 tests/overlay_bad_fixup_empty_index.dts
 tests/overlay_bad_fixup_index_trailing.dts
 tests/overlay_bad_fixup_path_empty_prop.dts
 tests/overlay_bad_fixup_path_only.dts
 tests/overlay_bad_fixup_path_only_sep.dts
 tests/overlay_bad_fixup_path_prop.dts
 tests/overlay_base.dts
 tests/overlay_base_manual_symbols.dts
 tests/overlay_overlay_no_fixups.dts
Copyright: 2016, NextThing Co
 2016, Free Electrons
License: GPL-2

Files: tests/overlay_base_phandle.dts
 tests/overlay_overlay_phandle.dts
Copyright: 2024, Uwe Kleine-König
License: GPL-2

Files: tests/overlay_overlay.dts
 tests/overlay_overlay_bypath.dts
 tests/overlay_overlay_local_merge.dts
 tests/overlay_overlay_manual_fixups.dts
 tests/overlay_overlay_nosugar.dts
Copyright: 2016, NextThing Co
 2016, Konsulko Inc.
 2016, Free Electrons
License: GPL-2

Files: tests/overlay_overlay_simple.dts
Copyright: 2016, Konsulko Inc.
License: GPL-2

Files: tests/path_offset_aliases.c
Copyright: 2008, Kumar Gala, Freescale Semiconductor, Inc.
 2006, David Gibson, IBM Corporation.
License: LGPL-2.1

Files: tests/property_iterate.c
 tests/subnode_iterate.c
Copyright: 2013, Google, Inc
 2007, David Gibson, IBM Corporation.
License: LGPL-2.1

Files: tests/propname_escapes.c
Copyright: 2012, NVIDIA CORPORATION.
 2006, David Gibson, IBM Corporation.
License: LGPL-2.1

Files: tests/relref_merge.c
Copyright: 2020, Ahmad Fatoum, Pengutronix.
 2006, David Gibson, IBM Corporation.
License: LGPL-2.1

Files: tests/stringlist.c
Copyright: 2015, NVIDIA Corporation
License: LGPL-2.1

Files: tests/utilfdt_test.c
Copyright: 2011, The Chromium Authors
License: LGPL-2.1

Files: util.c
 util.h
Copyright: 2011, The Chromium Authors
 2008, Jon Loeliger, Freescale Semiconductor, Inc.
License: GPL-2

Files: yamltree.c
Copyright: David Gibson <dwg@au1.ibm.com>, IBM Corporation. 2005.
 2018, Linaro, Ltd.
 2017, Arm Holdings.
License: GPL-2

Files: CONTRIBUTING.md Documentation/dtc-paper.bib pylibfdt/libfdt.i
Copyright:
 2005-2008 David Gibson <dwg@au1.ibm.com>, IBM Corporation.
 2007-2008 Jon Loeliger, Freescale Semiconductor, Inc.
 2011 The Chromium OS Authors.
 2007 Gerald Van Baren, Custom IDEAS, vanbaren@cideas.com
 2016 Free Electrons
 2016 Konsulko Inc.
 2016 NextThing Co
 2017 Konsulko Group Inc. All rights reserved.
 2008 Kumar Gala, Freescale Semiconductor, Inc.
 2009-2012 David Gibson, IBM Corporation.
 2012 Kim Phillips, Freescale Semiconductor.
 2012 NVIDIA CORPORATION.
 2013-2017 Google, Inc
 2014 David Gibson <david@gibson.dropbear.id.au>
 2018 AKASHI Takahiro, Linaro Limited
 2018 David Gibson
 2018 David Gibson, IBM Corporation.
 2018 David Gibson, Red Hat Inc.
 2018 embedded brains GmbH
 Arm Holdings.  2017
 Linaro, Ltd. 2018
License: GPL-2+

Files: libfdt/fdt.h libfdt/fdt_addresses.c libfdt/fdt_overlay.c libfdt/libfdt_env.h
Copyright:
 2006-2012 David Gibson, IBM Corporation.
 2012 Kim Phillips, Freescale Semiconductor.
 2014 David Gibson <david@gibson.dropbear.id.au>
 2016 Free Electrons
 2016 NextThing Co.
 2018 embedded brains GmbH
License: GPL-2+ or BSD-2-clause

Files: tests/pylibfdt_tests.py
Copyright:
 2017 Google, Inc. (Written by Simon Glass <sjg@chromium.org>)
License: GPL-2+ or BSD-2-clause
